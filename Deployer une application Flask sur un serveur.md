
# Comment déployer une application Flask sur serveur Debian

## Installer

Vous devez avoir Apache déjà installé et exécuté sur votre serveur.

## Étape 1 - Installer et activer mod_wsgi

WSGI (Web Server Gateway Interface) est une interface entre les serveurs Web et les applications Web pour Python. Mod_wsgi est un mod de serveur HTTP Apache qui permet à Apache de servir les applications Flask.

Ouvrez le terminal et tapez la commande suivante pour installer mod_wsgi:

    sudo apt-get install libapache2-mod-wsgi-py3 python3-dev 

Pour activer mod_wsgi, exécutez la commande suivante:

    sudo a2enmod wsgi 

## Deuxième étape - Création d'une application Flask

Dans cette étape, nous allons créer une application Flask. Nous placerons notre application dans le répertoire /var/www

Utilisez la commande suivante pour accéder au répertoire /var/www:

    cd /var/www 

Créez la structure du répertoire de l'application à l'aide de `mkdir` comme indiqué. Remplacer "FlaskApp" avec le nom que vous souhaitez donner à votre application. Créez le répertoire initial FlaskApp en donnant la commande suivante:

    sudo mkdir FlaskApp

Déplacez-vous dans ce répertoire à l'aide de la commande suivante:

    cd FlaskApp

Créez un autre répertoire FlaskApp en donnant la commande suivante:

    sudo mkdir FlaskApp

Ensuite, déplacez-vous dans ce répertoire et créez deux sous-répertoires nommés static et templates à l'aide des commandes suivantes:

    cd FlaskApp
    sudo mkdir static templates

La structure de votre répertoire devrait maintenant ressembler à ceci:

    |----FlaskApp
    |---------FlaskApp
    |--------------static
    |--------------templates

Maintenant, créez le fichier `__init__.py` qui contiendra la logique d'application de Flask.

    sudo nano __init__.py

Ajoutez la logique suivante au fichier:

    from flask import Flask
    app = Flask(__name__)
    @app.route("/")
    def hello():
        return "Bonjour Digifab"
    if __name__ == "__main__":
        app.run()

Enregistrez et fermez le fichier.

## Étape trois - Installer Flask

La configuration d'un _environnement virtuel_ maintiendra l'application et ses dépendances isolées du système principal. Les modifications apportées n'affecteront pas les configurations système du serveur cloud.

Dans cette étape, nous allons créer un environnement virtuel pour notre application Flask.

Nous utiliserons `pip`_ pour installer `virtualenv` et Flask . Si `pip` n'est pas installé, installez-le sur Ubuntu via `apt-get`.

    sudo apt-get install python3-pip

Si `virtualenv` n'est pas installé, utilisez `pip` pour l'installer à l'aide de la commande suivante:

    sudo pip install virtualenv

Exécutez la commande suivante (où venv est le nom que vous souhaitez donner à votre environnement temporaire):

    sudo virtualenv venv

Maintenant, installez Flask dans cet environnement en activant l'environnement virtuel avec la commande suivante:

    source venv/bin/activate

Exécutez cette commande pour installer Flask à l'intérieur:

    sudo pip install Flask 

Ensuite, exécutez la commande suivante pour tester si l'installation a réussi et si l'application est en cours d'exécution:

    sudo python __init__.py 

Il doit afficher «Running on http://localhost:5000/» ou «Running on http://127.0.0.1:5000/». Si vous voyez ce message, vous avez correctement configuré l'application.

Pour désactiver l'environnement, exécutez la commande suivante:

    deactivate

## Étape quatre - Configurer et activer un nouvel hôte virtuel

Émettez la commande suivante dans votre terminal:

    sudo nano /etc/apache2/sites-available/FlaskApp.conf

Ajoutez les lignes de code suivantes au fichier pour configurer l'hôte virtuel. Assurez-vous de changer le ServerName en adresse IP de votre serveur:

    <VirtualHost *:80>
    		ServerName mywebsite.com
    		ServerAdmin admin@mywebsite.com
    		WSGIScriptAlias / /var/www/FlaskApp/flaskapp.wsgi
    		<Directory /var/www/FlaskApp/FlaskApp/>
    			Order allow,deny
    			Allow from all
    		</Directory>
    		Alias /static /var/www/FlaskApp/FlaskApp/static
    		<Directory /var/www/FlaskApp/FlaskApp/static/>
    			Order allow,deny
    			Allow from all
    		</Directory>
    		ErrorLog ${APACHE_LOG_DIR}/error.log
    		LogLevel warn
    		CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

Enregistrez et fermez le fichier.

Activez l'hôte virtuel avec la commande suivante:

    sudo a2ensite FlaskApp

## Étape 5 - Créer le fichier .wsgi

Apache utilise le fichier .wsgi pour servir l'application Flask. Déplacer vers le répertoire /var/www/FlaskApp et créer un fichier nommé flaskapp.wsgi avec les commandes suivantes:

    cd /var/www/FlaskApp
    sudo nano flaskapp.wsgi

Ajoutez les lignes de code suivantes au fichier flaskapp.wsgi:

    #!/usr/bin/python
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr)
    sys.path.insert(0,"/var/www/FlaskApp/")
    
    from FlaskApp import app as application
    application.secret_key = 'Ajouter votre clef secrete'

Maintenant, la structure de votre répertoire devrait ressembler à ceci:

    | --------FlaskApp 
    |----------------FlaskApp
    | -----------------------static
    | -----------------------templates
    | -----------------------venv 
    | -----------------------__ init__.py  
    | ----------------flaskapp.wsgi

## Étape six - Redémarrer Apache

Redémarrez Apache avec la commande suivante pour appliquer les modifications:

    sudo service apache2 restart 

Un message semblable au suivant peut s'afficher:

> Could not reliably determine the VPS's fully qualified domain name,
> using 127.0.0.1 for ServerName

Ce message n'est qu'un avertissement et vous pourrez accéder à votre hôte virtuel sans aucun autre problème. Pour afficher votre application, ouvrez votre navigateur et accédez au nom de domaine ou à l'adresse IP que vous avez entré dans votre configuration d'hôte virtuel.

Vous avez déployé avec succès une application flask.
